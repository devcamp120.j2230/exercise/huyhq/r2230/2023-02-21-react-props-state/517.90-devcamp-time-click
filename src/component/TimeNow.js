import { Component } from "react";

class TimeNow extends Component{
    constructor(props){
        super(props);
        this.state = {
            time: [this.getTimeNow()],
        };

    };

    addTime = ()=>{
        this.setState({
            time: [...this.state.time, this.getTimeNow()]
        })
    };

    getTimeNow = ()=>{
        var date = new Date();
        return date.getHours()+":"+date.getMinutes()+":"+date.getSeconds();
    };

    render(){
        return(
            <>
            <ul>
                List:
                {this.state.time.map((val, i)=>{
                    return <li key={i}>{val}</li>
                })}
            </ul>
            <button onClick={this.addTime}>Add time</button>
            </>
        )
    }
}

export default TimeNow